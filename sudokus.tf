
resource "aws_s3_bucket" "sudoku_storage" {
  bucket = var.bucket_name
  acl    = "private"
}

data "aws_caller_identity" "current" {}

# Lambda

data "archive_file" "lambda" {
  type        = "zip"
  source_file = "${path.module}/lambda.py"
  output_path = "${path.module}/lambda.zip"
}

resource "aws_lambda_function" "lambda" {
  filename      = data.archive_file.lambda.output_path
  function_name = "sudoku"
  role          = aws_iam_role.sudoku_lambda.arn
  handler       = "lambda.lambda_handler"
  runtime       = "python3.8"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda.zip"))}"
  source_code_hash = filebase64sha256(data.archive_file.lambda.output_path)
}

# IAM
resource "aws_iam_role" "sudoku_lambda" {
  name = "sudoku_lambda"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}


data "aws_iam_policy_document" "lambda_logs" {
  statement {
    sid = "CreateLogGroup"

    actions = [
      "logs:CreateLogGroup",
    ]

    resources = [
      "arn:aws:logs:eu-west-1:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
  statement {
    sid = "CreateLogs"

    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    resources = [
      "arn:aws:logs:eu-west-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/*:*"
    ]
  }
}

resource "aws_iam_policy" "sudoku_lambda_log" {
  name   = "sudoku_lambda_log"
  path   = "/"
  policy = data.aws_iam_policy_document.lambda_logs.json
}


resource "aws_iam_role_policy_attachment" "sudoku_lambda_log" {
  role       = aws_iam_role.sudoku_lambda.name
  policy_arn = aws_iam_policy.sudoku_lambda_log.arn
}


data "aws_iam_policy_document" "s3_access" {
  statement {
    sid = "ListBucket"

    actions = [
      "s3:ListBucket",
    ]

    resources = [
      "arn:aws:s3:::${var.bucket_name}",
    ]
  }
  statement {
    sid = "AccessObjects"

    actions = [
      "s3:GetObject",
    ]

    resources = [
      "arn:aws:s3:::${var.bucket_name}",
    ]
  }
}

resource "aws_iam_policy" "access_sudoku_s3" {
  name   = "access_sudokus"
  path   = "/"
  policy = data.aws_iam_policy_document.s3_access.json
}


resource "aws_iam_role_policy_attachment" "sudoku_s3_access" {
  role       = aws_iam_role.sudoku_lambda.name
  policy_arn = aws_iam_policy.access_sudoku_s3.arn
}


#######################################################################
##################### API Gateway Configuration #######################
#######################################################################
resource "aws_api_gateway_rest_api" "sudokus" {
  name        = "sudoku-api"
  description = "Proxy to handle requests to get s3 buckets"
}

resource "aws_api_gateway_resource" "sudoku" {
  for_each    = var.sudoku_api_endpoints
  rest_api_id = aws_api_gateway_rest_api.sudokus.id
  parent_id   = aws_api_gateway_rest_api.sudokus.root_resource_id
  path_part   = each.key
}

resource "aws_api_gateway_method" "POST" {
  for_each      = var.sudoku_api_endpoints
  rest_api_id   = aws_api_gateway_rest_api.sudokus.id
  resource_id   = aws_api_gateway_resource.sudoku[each.key].id
  http_method   = "POST"
  authorization = "NONE"
  request_parameters = {
    "method.request.querystring.id" = false
  }
}

resource "aws_api_gateway_integration" "sudokus_lambda" {
  for_each                = var.sudoku_api_endpoints
  rest_api_id             = aws_api_gateway_rest_api.sudokus.id
  resource_id             = aws_api_gateway_resource.sudoku[each.key].id
  http_method             = aws_api_gateway_method.POST[each.key].http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda.invoke_arn
}

resource "aws_api_gateway_domain_name" "sudokus" {
  certificate_arn = aws_acm_certificate_validation.sudokus.certificate_arn
  domain_name     = aws_acm_certificate.sudokus.domain_name
}

resource "aws_api_gateway_deployment" "sudokus" {
  rest_api_id = aws_api_gateway_rest_api.sudokus.id

  triggers = {
    redeployment = sha1(jsonencode(aws_api_gateway_rest_api.sudokus.body))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "sudokus" {
  deployment_id = aws_api_gateway_deployment.sudokus.id
  rest_api_id   = aws_api_gateway_rest_api.sudokus.id
  stage_name    = "default"
}

resource "aws_api_gateway_base_path_mapping" "sudokus" {
  api_id      = aws_api_gateway_rest_api.sudokus.id
  stage_name  = aws_api_gateway_stage.sudokus.stage_name
  domain_name = aws_api_gateway_domain_name.sudokus.domain_name
}

################################################################
################### CORS INTEGRATION ###########################
################################################################

# aws_api_gateway_method.OPTIONS
resource "aws_api_gateway_method" "OPTIONS" {
  for_each      = var.sudoku_api_endpoints
  rest_api_id   = aws_api_gateway_rest_api.sudokus.id
  resource_id   = aws_api_gateway_resource.sudoku[each.key].id
  http_method   = "OPTIONS"
  authorization = "NONE"
}

# aws_api_gateway_integration._
resource "aws_api_gateway_integration" "_" {
  for_each         = var.sudoku_api_endpoints
  rest_api_id      = aws_api_gateway_rest_api.sudokus.id
  resource_id      = aws_api_gateway_resource.sudoku[each.key].id
  http_method      = aws_api_gateway_method.OPTIONS[each.key].http_method
  content_handling = "CONVERT_TO_TEXT"

  type = "MOCK"

  request_templates = {
    "application/json" = "{ \"statusCode\": 200 }"
  }
}

# aws_api_gateway_integration_response._
resource "aws_api_gateway_integration_response" "_" {
  for_each    = var.sudoku_api_endpoints
  rest_api_id = aws_api_gateway_rest_api.sudokus.id
  resource_id = aws_api_gateway_resource.sudoku[each.key].id
  http_method = aws_api_gateway_method.OPTIONS[each.key].http_method
  status_code = 200

  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'",
    "method.response.header.Access-Control-Allow-Methods" = "'OPTIONS,POST'",
    "method.response.header.Access-Control-Allow-Origin"  = "'*'"
  }
}

# aws_api_gateway_method_response._
resource "aws_api_gateway_method_response" "_" {
  for_each    = var.sudoku_api_endpoints
  rest_api_id = aws_api_gateway_rest_api.sudokus.id
  resource_id = aws_api_gateway_resource.sudoku[each.key].id
  http_method = aws_api_gateway_method.OPTIONS[each.key].http_method
  status_code = 200

  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = false
    "method.response.header.Access-Control-Allow-Methods" = false
    "method.response.header.Access-Control-Allow-Origin"  = false
  }

  response_models = {
    "application/json" = "Empty"
  }

}

resource "aws_api_gateway_gateway_response" "response_4xx" {
  rest_api_id   = aws_api_gateway_rest_api.sudokus.id
  response_type = "DEFAULT_4XX"

  response_templates = {
    "application/json" = "{'message':$context.error.messageString}"
  }

  response_parameters = {
    "gatewayresponse.header.Access-Control-Allow-Origin" = "'*'"
  }
}

resource "aws_api_gateway_gateway_response" "response_5xx" {
  rest_api_id   = aws_api_gateway_rest_api.sudokus.id
  response_type = "DEFAULT_5XX"

  response_templates = {
    "application/json" = "{'message':$context.error.messageString}"
  }

  response_parameters = {
    "gatewayresponse.header.Access-Control-Allow-Origin" = "'*'"
  }
}

################################################################
################################################################
################################################################

resource "aws_lambda_permission" "apigw" {
  for_each      = var.sudoku_api_endpoints
  statement_id  = "AllowExecutionFromAPIGateway${each.key}"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = format("arn:aws:execute-api:eu-west-1:%s:%s/*/%s%s",
    data.aws_caller_identity.current.account_id,
    aws_api_gateway_rest_api.sudokus.id,
    aws_api_gateway_method.POST[each.key].http_method,
    aws_api_gateway_resource.sudoku[each.key].path
  )
}


#######################################################################
#################### DNS record configuration #########################
#######################################################################

resource "aws_acm_certificate_validation" "sudokus" {
  provider                = aws.NorthAmerica
  certificate_arn         = aws_acm_certificate.sudokus.arn
  validation_record_fqdns = [for record in aws_route53_record.validation : record.fqdn]
}

resource "aws_acm_certificate" "sudokus" {
  provider          = aws.NorthAmerica
  domain_name       = "sudokus.andrewjohnperry.com"
  validation_method = "DNS"
}

data "aws_route53_zone" "website" {
  name         = "andrewjohnperry.com"
  private_zone = false
}

resource "aws_route53_record" "validation" {
  for_each = {
    for dvo in aws_acm_certificate.sudokus.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.website.zone_id
}

resource "aws_route53_record" "sudokus" {
  name    = aws_api_gateway_domain_name.sudokus.domain_name
  type    = "A"
  zone_id = data.aws_route53_zone.website.zone_id

  alias {
    evaluate_target_health = true
    name                   = aws_api_gateway_domain_name.sudokus.cloudfront_domain_name
    zone_id                = aws_api_gateway_domain_name.sudokus.cloudfront_zone_id
  }
}


