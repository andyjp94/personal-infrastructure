variable "do_token" {}

variable "enable_kubernetes_cluster" {
  default = 0
}

variable "bucket_name" {
  description = "The name of the bucket"
  default     = "sudokus-andrewjohnperry.com"
}

variable "sudoku_api_endpoints" {
  description = "The names of the sudoku API endpoints"
  default     = ["problem", "solution"]
  type        = set(string)
}
