data "digitalocean_kubernetes_versions" "one_eighteen" {
  version_prefix = "1.18."
}

resource "digitalocean_kubernetes_cluster" "default" {
  count        = var.enable_kubernetes_cluster
  name         = "default"
  region       = "lon1"
  auto_upgrade = true
  version      = data.digitalocean_kubernetes_versions.one_eighteen.latest_version

  node_pool {
    name       = "autoscale-worker-pool"
    size       = "s-2vcpu-2gb"
    auto_scale = true
    min_nodes  = 1
    max_nodes  = 2
  }
}