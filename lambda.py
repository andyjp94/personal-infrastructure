#!/usr/bin/env python3
import json
import sys
from io import StringIO
import boto3
import pprint
import secrets


def get_solution(s3):
    paginator = s3.get_paginator("list_objects")
    prefix = "solutions/complete_"
    response_iterator = paginator.paginate(
        Bucket="sudokus-andrewjohnperry.com",
        Prefix=prefix,
        PaginationConfig={"MaxItems": 123, "PageSize": 123},
    )
    files = []
    for resp in response_iterator:
        files = [files["Key"] for files in resp["Contents"]]
    objectName = secrets.choice(files)
    # Gets the ID from the name of the file, the -4 is because the extension is .csv
    id = objectName[len(prefix) : -4]
    obj = s3.get_object(
        Bucket="sudokus-andrewjohnperry.com",
        Key=objectName,
    )
    return {
        "statusCode": 200,
        "headers": {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
        },
        "body": json.dumps(
            {"sudoku": obj["Body"].read().decode("utf-8"), "id": id},
        ),
    }


def get_problem(s3, solution):
    paginator = s3.get_paginator("list_objects")
    response_iterator = paginator.paginate(
        Bucket="sudokus-andrewjohnperry.com",
        Prefix="problems/problem_" + solution,
        PaginationConfig={"MaxItems": 123, "PageSize": 123},
    )
    files = []
    for resp in response_iterator:
        files = [files["Key"] for files in resp["Contents"]]

    obj = s3.get_object(
        Bucket="sudokus-andrewjohnperry.com",
        Key=secrets.choice(files),
    )
    return {
        "statusCode": 200,
        "headers": {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
        },
        "body": json.dumps(obj["Body"].read().decode("utf-8")),
    }


def lambda_handler(event, context):
    s3 = boto3.client("s3")
    if event["path"] == "/solution":
        result = get_solution(s3)
    elif event["path"] == "/problem":
        id = event["queryStringParameters"]["id"]
        result = get_problem(s3, id)

    return result
