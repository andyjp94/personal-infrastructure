terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
  }
  required_version = ">= 0.15"
}

provider "aws" {
  region = "eu-west-1"
}

provider "aws" {
  alias  = "NorthAmerica"
  region = "us-east-1"
}
